/*
Functions:
1. FOLLOW TRACK
2. IF COUNTER > 3 ==> STOP
3. USES STATE MACHINE, START WITH HEAT, PWM COTROLS MOTORS (SPEED MANAGMENT)
4. OWN SENSOR (START PROGRAM ON TEMPETURE CHANGE), FUNCTION STYLE, INTERRUPTIONS

Creators:
Valtteri Seuranen 
Leevi Päivärinta

*/
#include "msp430g2553.h" 
#define true 1;
#define false 0;

int STATE;
#define STARTUP 0
#define MOVE 1
#define STANDBY 2
#define COMPLETED 3
#define ERROR 4

#define MOTOR_L BIT6
#define MOTOR_R BIT1

#define STOP 0x00
#define SLOW 0x50
#define AVERAGE 0xcc
#define FAST 0xff


//Sensor input masks (0 => none / 1 => input)
unsigned int left, middle, right;
unsigned int lap, lastDirection, atFinishline;
long temp, max;

// ADC10 interrupt service routine
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR (void) {
    temp = ADC10MEM;
    max = 745; 
    if(temp > max){
        STATE = MOVE;
    }
    __bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
}

//Button 1.3 is pressed
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
  switch(STATE)
  {
  case STARTUP:
  P1OUT |= BIT0;
  STATE = MOVE;
  break;
  
  case MOVE:
  P1OUT |= ~0x00;
  STATE = STARTUP;
  break;
  
  case STANDBY:
  STATE = MOVE;
  break;
  
  case COMPLETED:
  P1OUT |= ~0x00;
  STATE = STARTUP;
  break;
  
  case ERROR:
  P1OUT |= ~0x00;
  STATE = STARTUP;
  break;
  }
  P1IFG = 0x00; //reset interrupt flag
}
¨
//find track when it has been lost
void findTrack(){
    //return back to track
    
    if(lastDirection == 1){
        //was going rigth --> lets go left

        while(right == 0){
              
              if((P2IN & BIT0) != 0){
                right = false;
              } else {
                right = true;
              }
              
                  TA0CCR1 = SLOW;
                  TA1CCR1 = FAST;
            }
        
    } else if(lastDirection == 2){
        while(left == 0){
              
               if((P1IN & BIT4) != 0){
                left = false;
                } else {
                    left = true;
                }
               
                  TA0CCR1 = FAST;
                 TA1CCR1 = SLOW;
            }
    } else {
        TA0CCR1 = 0x00;
        TA1CCR1 = 0x00;
    }
}

//Move based on sensor data
void sensorMovement(){
    
  
     if((left && middle && right))
        {
            //StartLine
            TA0CCR1 = FAST;
            TA1CCR1 = FAST;
            P1OUT ^= BIT0;
            
            if(lastDirection != 0 && atFinishline != 1){
                lap++;
            }
            atFinishline = 1;
            lastDirection = 0;
        }
        else if((left && (middle == 0) && (right == 0)))
        {
            TA0CCR1 = STOP;
            TA1CCR1 = FAST;
            lastDirection = 1;
            atFinishline = 0;
            P1OUT ^= BIT0;
        }
        else if(((left == 0) && (middle == 0) && right))
        {
            TA0CCR1 = FAST;
            TA1CCR1 = STOP;
            lastDirection = 2;
            atFinishline = 0;
                    P1OUT ^= BIT0;
        }
        else if(middle)
        {
            //line is at the center
            TA0CCR1 = FAST;
            TA1CCR1 = FAST;
            lastDirection = 3;
            atFinishline = 0;
                    P1OUT ^= BIT0;
        }
        else if((left == 0) && right && middle){
            TA0CCR1 = FAST;
            TA1CCR1 = SLOW;
            lastDirection = 2;
                    P1OUT ^= BIT0;
        
        }
        else if(left && (right == 0) && middle){
            TA0CCR1 = SLOW;
            TA1CCR1 = FAST;
            lastDirection = 1;
                    P1OUT ^= BIT0;
        }
        else
        {
            /*
             * else when:
             * left and right are on but middle is not ==> error?
             * all sensor go zero ==> out of track
             *
             */
                    P1OUT ^= BIT0;
            findTrack();
            //P1OUT = ~MOTOR_L;
            //P2OUT = ~MOTOR_R;
            //STATE = ERROR;
        }
}

//Read data from sensors
void sensorUpdate(){

  //set up sensor reading
    if((P1IN & BIT4) != 0){
        left = false;
    } else {
        left = true;
    }

    if((P1IN & BIT5) != 0){
        middle = false;
    } else {
        middle = true;
    }

    if((P2IN & BIT0) != 0){
        right = false;
    } else {
        right = true;
    }
}

 main()
{
    WDTCTL = WDTPW + WDTHOLD; //stop watchdog
    P1DIR = MOTOR_L + BIT0;
    P2DIR = MOTOR_R;
    
    P1OUT &= ~MOTOR_L; //P1.3
    P2OUT &= ~MOTOR_R; //P2.1

    //startUp Values
    lap = 0;
    lastDirection = 0;
    atFinishline = 1;
    STATE = STARTUP;
    
    //Task 3 initialization
    //setting up timers

    //Set timers to ports
    P1SEL |= MOTOR_L;
    P2SEL |= MOTOR_R;

    //Timer0_A
    TA0CTL =  TASSEL_2 + MC_1 + ID_0; //SMCLK, UP_MODE, DIVIDER 0
    TA0CCTL1 = OUTMOD_7; //SET/RESET_MODE
    TA0CCR0 = 0xFF;

    //Timer0_B
    TA1CTL =  TASSEL_2 + MC_1 + ID_0; //SMCLK, UP_MODE, DIVIDER 0
    TA1CCTL1 = OUTMOD_7; //SET/RESET_MODE
    TA1CCR0 = 0xFF;

    //set timers to 0 as default
    TA0CCR1 = 0x00;
    TA1CCR1 = 0x00;
    
    P1OUT &= ~BIT0;
    
    
    P1REN |= BIT3; //P1.4 Pullup pulldown resistor enabled or pulldown
    P1IFG = 0x00; //reset interruptflag
    P1IES |= BIT3; //Portin 1.4 interrupt if change from high to low
    P1IE |= BIT3; //1.4 enable interrupt
    
    //internal heat sensor
    ADC10CTL1 = INCH_10 + ADC10DIV_3;         // Temp Sensor ADC10CLK/4
    ADC10CTL0 = SREF_1 + ADC10SHT_3 + REFON + ADC10ON + ADC10IE;
    __enable_interrupt();
    
    atFinishline = 1;

    while(1){
        switch(STATE)
        {
            case STARTUP:
                //aloitetaan l?mp?tilan tarkastelu
                ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
                __bis_SR_register(CPUOFF + GIE);        // LPM0 with interrupts enabled
            break;
            case MOVE:
                
                if(lap < 3){
                    sensorUpdate();
                    sensorMovement();
                } else {
                    STATE = COMPLETED;
                }
            break;
            case STANDBY:
                TA0CCR1 = 0x00;
                TA1CCR1 = 0x00;
                P1OUT ^= BIT0;
            break;
            case COMPLETED:
                TA0CCR1 = 0x00;
                TA1CCR1 = 0x00;
                P1OUT ^= BIT0;
                lap = 0;
            break;
            default:
                //start reading tempeture changes
                ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
                __bis_SR_register(CPUOFF + GIE);        // LPM0 with interrupts enabled
            break;
        }
    }
}
