Project for Embedded programming course.

1. In electric engineering course we created a circuitboard for existing robotcar.
    
    robotcar:
    * 2 motors (left and right)
    * 3 Sensor side-by-side at the front for reading directions
    * msp430g2553 circuitboard for logic
    * Batteries for power

2. In Embedded programming course we programmed the car follow a existing track (made from black tape on a wooden board)
3. Added additional features to the program

<img src="illustration.jpg" width="400"/>
