/*
Program functions:
1. Car follows track
2. Does 3 laps and then stop
3. Uses a state machine, starts with heat, PWM used to control motor speed --> timers sent pulses durning defined intervals --> car moves
4. The code uses function style, when P1.3 button is pressed --> manually changes statemachines state
5. NOT IMPLEMENTED: Records cars movements durning drive, after completing all laps car repeats the laps from memory.
*/

#include "msp430g2553.h"
#define true 1;
#define false 0;

//defining program states
#define STARTUP 0
#define MOVE 1
#define STANDBY 2
#define COMPLETED 3
#define ERROR 4
#define FLASH_MOVE 5
#define FLASH_COMPLETE 6

//Motor output ports
#define MOTOR_L BIT6
#define MOTOR_R BIT1

//Motor speed options
#define STOP 0x00
#define SLOW 0x50
#define AVERAGE 0xcc
#define FAST 0xf0

//Sensor input masks (0 => none / 1 => input)
unsigned int left, middle, right;
int STATE;
unsigned int lap, lastDirection, currentDirection, atFinishline;

//for reading and writing flash
int *flash_addr; //
int *addr_begin; //static flash variable starts
int addressCount; //variables saved to flash

//tempeture setup:
long temp, max;

// ADC10 heat-stopping-routine
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR (void)
{
    temp = ADC10MEM;
    max = 742;

    if(temp > max)
    {
        P1OUT ^= BIT0;
		
		if(STATE == STARTUP)
		{
            STATE = MOVE;
		}
		else
		{
            STATE = FLASH_MOVE;
		}
        
    }
    __bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
}

//When button connected to port 1.3 activated, set program to change state to next state:
//startup-->move-->completed-->FLASH_MOVE-->FLASH_COMPLETE-->startup-->move...
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
  switch(STATE)
  {
  case STARTUP:
      STATE = MOVE;
          break;

      case MOVE:
      STATE = COMPLETED;
          break;

      case STANDBY:
      STATE = COMPLETED;
          break;

      case COMPLETED:
      STATE = FLASH_MOVE;
         break;

      case ERROR:
      STATE = STARTUP;
          break;

      case FLASH_MOVE:
        STATE = FLASH_COMPLETE;
        break;

      case FLASH_COMPLETE:
        STATE = STARTUP;
        break;
  }
  P1IFG = 0x00; //reset flag
}

//clear flash memory
void flash_erase(int *addr) 
{
  __disable_interrupt();

  addressCount = 0;

  while(BUSY & FCTL3);
  FCTL2 = FWKEY + FSSEL_1 + FN3;
  FCTL1 = FWKEY + ERASE;
  FCTL3 = FWKEY;
  *addr = 0;
  while(BUSY & FCTL3);
  FCTL1 = FWKEY;
  FCTL3 = FWKEY + LOCK;
  __enable_interrupt();
}

//write values to flash memory
void flash_write(int value) 
{
  
  __disable_interrupt();
  FCTL2 = FWKEY + FSSEL_1 + FN0;      
  FCTL3 = FWKEY;                       
  FCTL1 = FWKEY + WRT;                

  *flash_addr++ = value;
  addressCount++;

  FCTL1 = FWKEY;                    
  FCTL3 = FWKEY + LOCK;             
  while(BUSY & FCTL3);
  __enable_interrupt();
  
}

void flashMovement(void)
{

      int counter = addressCount;
      *flash_addr = *addr_begin;
      int direction = 0;

      while(counter > 0){
          direction = *addr_begin;

          switch(direction)
          {
          case 0:
              //startline
                  TA0CCR1 = FAST;
                  TA1CCR1 = FAST;
              break;
          case 1:
              //turning left
                  TA0CCR1 = STOP;
                  TA1CCR1 = FAST;
              break;
          case 2:
              //turning right
                  TA0CCR1 = FAST;
                  TA1CCR1 = STOP;
              break;
          case 3:
              //going straight
                  TA0CCR1 = FAST;
                  TA1CCR1 = FAST;
              break;
          }
          counter--;
          *addr_begin++;
      }

      STATE = FLASH_COMPLETE;
}

//When track is lost start moving to other direction from where we used to be going
void findTrack()
{
    if(lastDirection == 1){
        //lets go left
        while(right == 0){

              if((P2IN & BIT0) != 0){
                right = false;
              } else {
                right = true;
              }
                  flash_write(2);
                  TA0CCR1 = SLOW;
                  TA1CCR1 = FAST;
            }
    } else if(lastDirection == 2){
        while(left == 0){

               if((P1IN & BIT4) != 0){
                left = false;
                } else {
                    left = true;
                }

                flash_write(1);
                TA0CCR1 = FAST;
                TA1CCR1 = SLOW;
            }
    } else {
        TA0CCR1 = 0x00;
        TA1CCR1 = 0x00;
    }
}

//Move based on sensor data
void sensorMovement()
{
     if((left && middle && right))
        {
            //We are at the startLine
            TA0CCR1 = FAST;
            TA1CCR1 = FAST;

            //increase lap only if we just arrived to a finish line
            if(lastDirection != 0 && atFinishline != 1){
                lap++;
            }

            flash_write(0);
            atFinishline = 1;
            lastDirection = 0;
        }
        else if((left && (middle == 0) && (right == 0)))
        {
            TA0CCR1 = STOP;
            TA1CCR1 = FAST;

            flash_write(1);
            lastDirection = 1;
            atFinishline = 0;
        }
        else if(((left == 0) && (middle == 0) && right))
        {
            TA0CCR1 = FAST;
            TA1CCR1 = STOP;

            flash_write(2);
            lastDirection = 2;
            atFinishline = 0;
        }
        else if(middle)
        {
            //line is at the center
            TA0CCR1 = FAST;
            TA1CCR1 = FAST;

            flash_write(3);
            lastDirection = 3;
            atFinishline = 0;
        }
        else if((left == 0) && right && middle){
            TA0CCR1 = FAST;
            TA1CCR1 = SLOW;

            flash_write(2);
            lastDirection = 2;

        }
        else if(left && (right == 0) && middle){
            TA0CCR1 = SLOW;
            TA1CCR1 = FAST;

            flash_write(2);
            lastDirection = 1;
        }
        else
        {
            /*
             * else when:
             * left and right are on but middle is not ==> error?
             * all sensor go zero ==> out of track
             *
             */
            findTrack();
        }
}

void stopMotor()
{
    TA0CCR1 = 0x00;
    TA1CCR1 = 0x00;
}

//Read current status of sensors
void sensorUpdate()
{
  //Sensors detect not black but white so if sensor is 1 then sensor is top of white.
  //Set up sensor reading
    if((P1IN & BIT4) != 0){
        left = false;
    } else {
        left = true;
    }

    if((P1IN & BIT5) != 0){
        middle = false;
    } else {
        middle = true;
    }

    if((P2IN & BIT0) != 0){
        right = false;
    } else {
        right = true;
    }
}

void main()
{

        WDTCTL = WDTPW + WDTHOLD; //stop watchdog
        P1DIR = MOTOR_L + BIT0;
        P2DIR = MOTOR_R;

        P1OUT &= ~MOTOR_L; //P1.3
        P2OUT &= ~MOTOR_R; //P2.1

        //startUp Values
        lap = 0;
        lastDirection = 0;
        atFinishline = 1;
        STATE = STARTUP;

        //Set timers to motors
        P1SEL |= MOTOR_L;
        P2SEL |= MOTOR_R;

        //Timer0_A
        TA0CTL =  TASSEL_2 + MC_1 + ID_0; //SMCLK, UP_MODE, DIVIDER 0
        TA0CCTL1 = OUTMOD_7; //SET/RESET_MODE
        TA0CCR0 = 0xFF; 

        //Timer0_B
        TA1CTL =  TASSEL_2 + MC_1 + ID_0; //SMCLK, UP_MODE, DIVIDER 0
        TA1CCTL1 = OUTMOD_7; //SET/RESET_MODE
        TA1CCR0 = 0xFF; 

        //set timers to 0 as default
        stopMotor();
        
        P1OUT &= ~BIT0;

        P1REN |= BIT3; //P1.4 Pullup pulldown resistor enabled or pulldown
        P1IFG = 0x00; //reset interruptflag
        P1IES |= BIT3; //PORT1.4 interrupt if change from Hi to Low
        P1IE |= BIT3; //1.4 interrupt enabled


        //internal heat sensor
        ADC10CTL1 = INCH_10 + ADC10DIV_3;         // Temp Sensor ADC10CLK/4
        ADC10CTL0 = SREF_1 + ADC10SHT_3 + REFON + ADC10ON + ADC10IE;
        __enable_interrupt();

        atFinishline = 1;
        flash_addr = (int *)0x0A000;
        addr_begin = (int *)0x0A000 ; // flash_addr of the flash memory segment starting

        /*
         * 1. Heat startup
         * 2. Move the course 3 times
         * 3. Stop
         * 4. On button click --> move based on memory
         * 5. Then standby
         * 6. Reset --> 1.
         */
	

      while(1){
        switch(STATE)
        {
            case STARTUP:
				
		flash_erase(flash_addr); //Clear flash memory from previous data		
                lap = 0;
                
                //start looking at changes in tempteture
                ADC10CTL0 |= ENC + ADC10SC;
                __bis_SR_register(CPUOFF + GIE);
                
            break;

            case MOVE:
                if(lap < 3)
                {
                    sensorUpdate();
                    sensorMovement();
                }
                else
                {
                    STATE = COMPLETED;
                }
            break;

            case STANDBY:
                stopMotor();
                P1OUT ^= BIT0;
                break;

            case COMPLETED:
                stopMotor();
                P1OUT ^= BIT0;
                break;

            case FLASH_MOVE:
                flashMovement();
                break;

            case FLASH_COMPLETE:
                stopMotor();
                P1OUT ^= BIT0;
				
		ADC10CTL0 |= ENC + ADC10SC;
                __bis_SR_register(CPUOFF + GIE);
				
                break;

            case ERROR:
                stopMotor();
                P1OUT ^= BIT0;
                break;

            default:
                ADC10CTL0 |= ENC + ADC10SC;
                __bis_SR_register(CPUOFF + GIE);
            break;
        }
    }
}
